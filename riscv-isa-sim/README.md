RISC-V ISA Simulator
======================

Variant of the isa simulator available at https://github.com/riscv/riscv-isa-sim/tree/3a4e89322a8c8dac94185812a238f13789ab392f
-------------

The RISC-V ISA Simulator implements a functional model of one or more
RISC-V processors.

Build Steps
---------------

We assume that the RISCV environment variable is set to the RISC-V tools
install path, and that the riscv-fesvr package is installed there.

    $ apt-get install device-tree-compiler
    $ mkdir build
    $ cd build
    $ ../configure --prefix=$RISCV --with-fesvr=$RISCV --enable-commitlog
    $ make
    $ [sudo] make install

